export type QuestionState = 'fresh' | 'open' | 'done';
