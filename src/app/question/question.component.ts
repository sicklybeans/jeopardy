import {
  Component,
  OnInit,
  EventEmitter,
  HostListener,
  Input,
  Output
} from '@angular/core';
import {QuestionState, QuestionType, Question, Category} from '../questions.model';


@Component({
  selector: 'jep-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  @Input()
  question: Question;

  @Input()
  isCategory: boolean;

  @Output()
  started = new EventEmitter<Question>();

  constructor() {}

  ngOnInit(): void {}

  showQuestion(event) {
    this.question.state = 'displayed';
    event.stopPropagation();
  }

  @HostListener('click')
  _onClicked(): void {
    if (this.question.state === 'fresh') {
      this.question.state = 'displayed';
      if (!this.question.finished && !this.isCategory) {
        this.question.finished = true;
        this.started.emit(this.question);
      }
    } else if (this.question.state === 'displayed') {
      if (this.isCategory) {
        this.question.state = 'fresh';
      } else {
        this.question.state = 'finished';
      }
    }
  }
}
