import { Input, Component, OnInit } from '@angular/core';

@Component({
  selector: 'jep-stopwatch',
  templateUrl: './stopwatch.component.html',
  styleUrls: ['./stopwatch.component.scss']
})
export class StopwatchComponent implements OnInit {

  FULL_TIME = 15.0;

  PART_TIME = 5.0;

  lastSetTime: number = this.FULL_TIME;

  timeLeft: number = 0.0;

  WARN_TIME = 3.0;

  private interval = null;

  @Input()
  controls: boolean;

  constructor() { }

  ngOnInit(): void {
    this.timeLeft = this.lastSetTime;
  }

  play() {
    this.stop();
    this.interval = setInterval(() => {
      if(this.timeLeft > 0) {
        this.timeLeft -= 0.1;
      } else {
        this.stop();
      }
    }, 100)
  }

  reset(timeLeft: number, stop: boolean = false) {
    this.stop();
    this.lastSetTime = timeLeft;
    this.timeLeft = timeLeft;
    if (!stop) {
      this.play();
    }
  }

  clear() {
    this.reset(0.0, true);
  }

  stop() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

}
