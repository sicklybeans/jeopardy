import {
  Component,
  OnInit,
  EventEmitter,
  HostListener,
  Input,
  Output
} from '@angular/core';
import {Round, QuestionType, QuestionState, Question, Category} from '../questions.model';

@Component({
  selector: 'jep-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit {

  @Input()
  round: Round;

  @Output()
  started = new EventEmitter<Question>();

  constructor() { }

  ngOnInit(): void {}

  onStarted(question: Question) {
    this.started.emit(question);
  }

}
