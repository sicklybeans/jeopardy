import {
  Component,
  OnInit,
  Input,
  Output
} from '@angular/core';
import {Player} from '../questions.model';

@Component({
  selector: 'jep-scoreboard',
  templateUrl: './scoreboard.component.html',
  styleUrls: ['./scoreboard.component.scss']
})
export class ScoreboardComponent implements OnInit {

  @Input()
  players: Player[];

  @Input()
  playerAnswered: boolean[];

  @Input()
  currentPlayer: number;

  @Input()
  currentAnswerer: number;

  @Input()
  nextPass: number;

  constructor() { }

  ngOnInit(): void {
  }

}
