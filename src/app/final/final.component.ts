import {
  Component,
  OnInit,
  Input,
  HostListener
} from '@angular/core';

@Component({
  selector: 'jep-final',
  templateUrl: './final.component.html',
  styleUrls: ['./final.component.scss']
})
export class FinalComponent implements OnInit {

  @Input()
  category: string;

  @Input()
  question: string;

  state: 'category' | 'question' = 'category';

  constructor() { }

  ngOnInit(): void {}

  @HostListener('click')
  _onClicked(): void {
    if (this.state === 'category') {
      this.state = 'question'
    }
  }
}
