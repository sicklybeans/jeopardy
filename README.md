# Angular Jeopardy

A very simple browser based trivia game based off of Jeopardy. This is designed to be played over video chat through screen sharing.

## Code Quality Disclaimer

This project was written in my spare time over a few days to allow my family to play Jeopardy over video chat. The code is sloppy since I had so little time. It does not reflect my typical coding standards and is in no way intended to be portable or distributed.

## Usage Instructions

To use this, you have to write a JSON file indicating the players and save it as `src/assets/players.json` and another JSON file containing the questions saved as `src/assets/questions`.
Questions can have one of three types: text, picture, or video. The picture and video question text should be a pathname of the image/video relative to `src`. For example, if your pictoral clues are stored in `src/assets/pictures/...`, you would use `src/assets/picture/question1.jpg` as the question text.

The exact format of `players.json` and `questions.json` is not documented here but can be easily reverse engineered from the code. The format badly needs a rewrite and refactor, but I don't care enough to do it.

To run the code:

    ng serve


